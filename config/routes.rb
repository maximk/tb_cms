Rails.application.routes.draw do

  namespace :admin do
   resources :pages do
     get :page_parts, :on => :collection
     get :clear, :on => :collection
     member do
       post 'preview'
     end
   end
   resources :snippets
 		resources :menus do
 			resources :menu_items
 		end
 		resources :contacts
 	end

  namespace :cms do
    resource :sitemap, :only => "show"
  end

  root :to => 'pages#show'

end


# This migration comes from tb_cms (originally 20120111134754)
class AddUseCustomUrlNameToSpudPages < ActiveRecord::Migration
  def change
    add_column :spud_pages, :use_custom_url_name, :boolean,:default => false
  end
end

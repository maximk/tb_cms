# This migration comes from tb_cms (originally 20120118141852)
class AddNotesToSpudPages < ActiveRecord::Migration
  def change
    add_column :spud_pages, :notes, :text
  end
end

# This migration comes from tb_core (originally 20140110040026)
class CreateSpudLiquidTags < ActiveRecord::Migration
  def change
    create_table :spud_liquid_tags do |t|
      t.integer :attachment_id
      t.string :attachment_type
      t.string :tag_name
      t.string :value
      t.timestamps
    end
    add_index :spud_liquid_tags, [:tag_name, :value]
  end
end

# This migration comes from tb_cms (originally 20120101193138)
class CreateSpudMenus < ActiveRecord::Migration
  def change
    create_table :spud_menus do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

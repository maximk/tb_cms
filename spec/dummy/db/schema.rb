# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140110051484) do

  create_table "spud_liquid_tags", force: true do |t|
    t.integer  "attachment_id"
    t.string   "attachment_type"
    t.string   "tag_name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_liquid_tags", ["tag_name", "value"], name: "index_spud_liquid_tags_on_tag_name_and_value", using: :btree

  create_table "spud_menu_items", force: true do |t|
    t.string   "parent_type"
    t.integer  "parent_id"
    t.integer  "item_type"
    t.integer  "spud_page_id"
    t.integer  "menu_order",   default: 0
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "spud_menu_id"
    t.string   "classes"
  end

  add_index "spud_menu_items", ["menu_order"], name: "index_spud_menu_items_on_menu_order", using: :btree
  add_index "spud_menu_items", ["parent_type", "parent_id"], name: "index_spud_menu_items_on_parent_type_and_parent_id", using: :btree
  add_index "spud_menu_items", ["spud_menu_id"], name: "index_spud_menu_items_on_spud_menu_id", using: :btree

  create_table "spud_menus", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "site_id",     default: 0, null: false
  end

  add_index "spud_menus", ["site_id"], name: "index_spud_menus_on_site_id", using: :btree

  create_table "spud_page_partial_revisions", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.string   "format"
    t.integer  "spud_page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_page_partial_revisions", ["spud_page_id", "name"], name: "revision_idx", using: :btree

  create_table "spud_page_partials", force: true do |t|
    t.integer  "spud_page_id"
    t.string   "name"
    t.text     "content"
    t.string   "format"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "symbol_name"
    t.text     "content_processed"
  end

  add_index "spud_page_partials", ["spud_page_id"], name: "index_spud_page_partials_on_spud_page_id", using: :btree

  create_table "spud_pages", force: true do |t|
    t.string   "name"
    t.string   "url_name"
    t.datetime "publish_at"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.string   "format",              default: "html"
    t.integer  "spud_page_id"
    t.text     "meta_description"
    t.string   "meta_keywords"
    t.integer  "page_order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "visibility",          default: 0
    t.boolean  "published",           default: true
    t.boolean  "use_custom_url_name", default: false
    t.text     "notes"
    t.integer  "site_id",             default: 0,      null: false
    t.string   "layout"
  end

  add_index "spud_pages", ["site_id"], name: "index_spud_pages_on_site_id", using: :btree

  create_table "spud_permalinks", force: true do |t|
    t.string   "url_name"
    t.string   "attachment_type"
    t.integer  "attachment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "site_id",         default: 0, null: false
  end

  add_index "spud_permalinks", ["attachment_type", "attachment_id"], name: "idx_permalink_attachment", using: :btree
  add_index "spud_permalinks", ["site_id"], name: "idx_permalinks_site_id", using: :btree

  create_table "spud_permissions", force: true do |t|
    t.string   "name",       null: false
    t.string   "tag",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_permissions", ["tag"], name: "index_spud_permissions_on_tag", unique: true, using: :btree

  create_table "spud_role_permissions", force: true do |t|
    t.integer  "spud_role_id",        null: false
    t.string   "spud_permission_tag", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_role_permissions", ["spud_permission_tag"], name: "index_spud_role_permissions_on_spud_permission_tag", using: :btree
  add_index "spud_role_permissions", ["spud_role_id"], name: "index_spud_role_permissions_on_spud_role_id", using: :btree

  create_table "spud_roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_snippets", force: true do |t|
    t.string   "name"
    t.text     "content"
    t.string   "format"
    t.text     "content_processed"
    t.integer  "site_id",           default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spud_snippets", ["name"], name: "index_spud_snippets_on_name", using: :btree
  add_index "spud_snippets", ["site_id"], name: "index_spud_snippets_on_site_id", using: :btree

  create_table "spud_user_settings", force: true do |t|
    t.integer  "spud_user_id"
    t.string   "key"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "super_admin"
    t.string   "login",                           null: false
    t.string   "email",                           null: false
    t.string   "crypted_password",                null: false
    t.string   "password_salt",                   null: false
    t.string   "persistence_token",               null: false
    t.string   "single_access_token",             null: false
    t.string   "perishable_token",                null: false
    t.integer  "login_count",         default: 0, null: false
    t.integer  "failed_login_count",  default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone"
    t.integer  "spud_role_id"
  end

  add_index "spud_users", ["email"], name: "index_spud_users_on_email", using: :btree
  add_index "spud_users", ["login"], name: "index_spud_users_on_login", using: :btree
  add_index "spud_users", ["spud_role_id"], name: "index_spud_users_on_spud_role_id", using: :btree

end

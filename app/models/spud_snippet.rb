class SpudSnippet < ActiveRecord::Base

  validates :name, :presence => true
  validates_uniqueness_of :name, :scope => :site_id

  scope :site, lambda {|sid| where(:site_id => sid)}

  acts_as_spud_liquid_content

  def postprocess_content
    template = Liquid::Template.parse(self.content)
    self.content_processed = template.render()
  end

  def content_processed=(content)
    write_attribute(:content_processed,content)
  end

  def content_processed
    if read_attribute(:content_processed).blank?
      self.update_column(:content_processed, postprocess_content)
    end
    return read_attribute(:content_processed)
  end

end

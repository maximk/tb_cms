# Change Log

## v1.1

- Support for Rails 4 and tb_core 1.2
- Fragment caching for pages and `sp_list_menu` helper

## v1.0.2

- Use the new Spud::NotFoundError for 404 handling in pages controller

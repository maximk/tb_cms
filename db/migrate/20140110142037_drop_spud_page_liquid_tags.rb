class DropSpudPageLiquidTags < ActiveRecord::Migration

  class SpudPageLiquidTag < ActiveRecord::Base
  end

  def up
    if defined?(SpudLiquidTag)
      SpudPageLiquidTag.all.each do |tag|
        SpudLiquidTag.create({
          :attachment_id => tag.attachment_id,
          :attachment_type => tag.attachment_type,
          :tag_name => tag.tag_name,
          :value => tag.value
        })
      end
    end
    drop_table :spud_page_liquid_tags
  end

  def down
    create_table :spud_page_liquid_tags do |t|
      t.integer :attachment_id
      t.string :attachment_type
      t.string :tag_name
      t.string :value
      t.timestamps
    end
    add_index :spud_page_liquid_tags, [:tag_name,:value]
    add_index :spud_page_liquid_tags, [:attachment_type,:attachment_id]
  end
end
